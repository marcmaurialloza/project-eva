# README #

Project EVA is a project for an Extensible Virtual Agent, that means an intelligent agent build in a way so new capabilities (perceptors, knowledge sources, behavior and actuators) can be added to it in form of new programs/libraries/plugins.

### How do I get set up? ###

* Pull this gradle project it will resolve the dependencies by itself.

### Technology Stack
* Java as Language
* Guice as DI Framework
* PF4J as Plugin Framework
* OpenDial as Dialog Manager (Perceptor/Actuator)
* Jenna or Pellet for RDF/OWL Knowledge Manager and Rules Engine
* JUnit as a Test Framework
* Mockito as Mock Framework

### Included perceptors ###
* OpenDial as dialog manager.

### Included knowledge and behavior managers ###
* Apache Jena as knowledge and behavior management system

### Included actuators ###
* OpenDial as dialog manager.

### Who do I talk to? ###

* Repo owner or admin