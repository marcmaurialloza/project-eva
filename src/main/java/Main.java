/**
 * Created by marcmaurialloza on 7/12/15.
 */

import opendial.DialogueSystem;

public class Main {

    public static void main(String args[]) {

        //Domain domain = XMLDomainReader.extractDomain("path/to/XML/domain/file");

        DialogueSystem system = new DialogueSystem();


// Adding new domain modules (optional)
        //system.attachModule(OneExampleOfNewModule.class);

// When used as part of another application, we often want to switch off the OpenDial GUI
        //system.getSettings().showGUI = false;

// Finally, start the system
        system.startSystem();

    }

}
